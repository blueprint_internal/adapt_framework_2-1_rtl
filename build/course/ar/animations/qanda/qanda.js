
function Qanda(resources)
{
	Qanda.resources = resources;
	Qanda.adapt = require('coreJS/adapt');

}
Qanda.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'Qanda', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},


	preload: function()
	{

        this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Qanda.resources.bckg);
		this.game.load.image('avatar', Qanda.resources.avatar);
		this.game.load.image('slab', Qanda.resources.slab);
		this.game.load.spritesheet('slabbtn', Qanda.resources.slabbtn, 650, 45);

	},

	create: function(evt)
	{
    //Call the animation build function.
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	textOff: function (evt){

	    for (var i=0;i<10;i++){ 

        var nowBut = this.butArray[i];

        nowBut.alpha = 0; 

		}
	}
	,


	up: function(evt)
			{

	}
		,
	over: function(evt)
			{

	}
		,
	out: function(evt)
			{

	}
	,

actionOnClick: function(evt){

     console.log("QANDA  Calling Remote Submission from actionOnClick");
	//evt.btnNum

	//SETS THE VARIABLES INSIDE THE REMOTE TEXT MODEL
    this.model = Qanda.adapt.findById(Qanda.resources.watchedID);

    //this.model = Qanda.adapt.findById(Qanda.resources.watchedID);
    //this.remotetextID = Qanda.resources.remotetextID;
    //This works
    //this.model.set('canvasText', Qanda.resources.ico1text); //["ico"+evt.btnNum+"text"]); 
	//Qanda.resources["ico"+evt.btnNum+"text"]); 
	//this dint work
	//var nowText = Qanda.resources["ico"+1+"text"];

	this.slablite.y = evt.y; 

	this.nowNum  = (evt.btnNum + 1);

 	this.model.set('canvasText', Qanda.resources["ico"+this.nowNum+"text"]);

 	this.model.set('remotetextID', Qanda.resources.remotetextID);


 	//this.remotetextID 

 	this.model.trigger('remoteSubmission');

	},

	buildAnimation: function()
	{

     //Background
     this.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.bckg.anchor.set(0.5);

    ///////////////////////////

    var headerDisplayStyle = {font:"20px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 550, align: "left",lineSpacing: -10 };

    var listDisplayStyle = {font:"24px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 700, align: "left",lineSpacing: -10 };

    //Textfield that displays the name of the lower icon.

         this.slabArray = new Array();
         this.localTextArray = Qanda.resources.iconArray;
		
		this.slablite = this.game.add.sprite(210, -1000, 'slab');
 
        for (var i=0;i<7;i++){ 

       	//this.slab1 = this.game.add.sprite(210, 33 + (i*50), 'slab');

       	this.slab1 = this.game.add.button( 210, 33 + (i*50), 'slabbtn', this.actionOnClick, this, 1, 0, 2);


       		this.slab1.alpha = 0;

			var speedAlpha = 100 + i*100;
			var tween1 = this.game.add.tween(this.slab1).to({alpha: 1}, 100, Phaser.Easing.Exponential.Out, true, speedAlpha);

		this.slab1.inputEnabled = true;
		//this.slab1.events.onInputDown.add(this.actionOnClick, this);
		this.slab1.btnNum = i;

		//btnNum
		this.slabArray.push(this.slab1);

		this.pfield_1 = this.game.add.text(230, 40 + (i*50), Qanda.resources.ico1text, headerDisplayStyle);
		this.pfield_1.setText(this.localTextArray[i]);

	    }

	    //Textfield that displays the name of the lower icon.
		//this.pfield_N = this.game.add.text(40, 800, Qanda.resources.ico1text, listDisplayStyle);
		//this.localText2Array = Qanda.resources.answerArray;
		//this.pfield_N.setText(this.localText2Array[0]);
 		

		this.avatarface = this.game.add.sprite(20, 20, 'avatar')
		this.avatarface.scale.x = .40;
		this.avatarface.scale.y = .40;
		//this.facepic.anchor.set(0.5);

		//this.icon001 = this.game.add.sprite(this.game.world.centerX, 570, 'icon001')
		//this.icon001.scale.x = .65;
		//this.icon001.scale.y = .65;
		//this.icon001.anchor.set(0.5);

	},

	butOff: function (evt){

//var temp = this.game.add.button( (this.xLoop * 180), 180 + (this.yLoop * 100), this.industryIconArray[i], this.actionOnClick, this, 1, 0, 2);
  
            /*
			if (this.xLoop < 3){
				this.xLoop += 1; 
			}else{
				this.yLoop += 1;
				this.xLoop = 1 ;
			}
			*/

            //This LAYS OUT THE BUTTONS ALONG THE BOTTOM
		//console.log("evt" + evt);

	    /*for (var i=0;i<this.countryDotArray.length;i++){ 

        var nowBut = this.countryDotArray[i];

        nowBut.alpha = 0; 

       //var nowParen = nowBut.parent;

		 //for (prop in nowParen){
          // console.log("prop : " + prop +" : "+ nowParen[prop]);
		//}

			//.textId.alpha = 0; 

		}*/

		//this.slab.anchor.set(0.5);

	/*this.localTextArray = Qanda.resources.iconArray;

	this.pfield_1 = this.game.add.text(230, 20, Qanda.resources.ico1text, headerDisplayStyle);
	this.pfield_2 = this.game.add.text(230, 70, Qanda.resources.ico1text, headerDisplayStyle);
    this.pfield_3 = this.game.add.text(230, 120, Qanda.resources.ico1text, headerDisplayStyle);
	this.pfield_4 = this.game.add.text(230, 170, Qanda.resources.ico1text, headerDisplayStyle);
	this.pfield_5 = this.game.add.text(230, 220, Qanda.resources.ico1text, headerDisplayStyle);
    this.pfield_6 = this.game.add.text(230, 270, Qanda.resources.ico1text, headerDisplayStyle);
    this.pfield_7 = this.game.add.text(230, 320, Qanda.resources.ico1text, headerDisplayStyle);


		//this.pfield_1.anchor.set(0.5);

		this.pfield_1.setText(this.localTextArray[0]);
		this.pfield_2.setText(this.localTextArray[1]);
		this.pfield_3.setText(this.localTextArray[2]);
		this.pfield_4.setText(this.localTextArray[3]);
		this.pfield_5.setText(this.localTextArray[4]);
		this.pfield_6.setText(this.localTextArray[5]);
		this.pfield_7.setText(this.localTextArray[6]);
		*/

		



	//this.butOn(evt);

	//for (prop in evt){
    // console.log("prop : " + prop +" : "+ evt[prop]);
    //}
	//console.log( " {btnText:'"+evt.textData+"' , xLoc:" + evt.x + " , yLoc:" + evt.y +" } ,")
/*
    this.redcirc.x = evt.x;
	this.redcirc.y = evt.y;

	this.redcirc.scale.x = .50;
	this.redcirc.scale.y = .50;

    this.pfield_2.setText("");

	this.testy2 = new Array();

	//this.icondotred.x = 0;
	//this.icondotred.y = 0;

    this.testy2 = this.indusArray[evt.btnId];

	this.localArray = Qanda.resources.iconArray;
	this.pfield_1.setText(this.localArray[evt.btnId]);

	this.pfield_2.setText(this.testy2);*/


	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}