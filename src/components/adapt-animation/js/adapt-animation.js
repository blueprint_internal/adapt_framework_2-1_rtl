/*
* adapt-animation
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <scallahan@fb.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var Animation = ComponentView.extend({

        events: {

        },

        preRender: function() {

        },

      postRender: function() {

       this.setReadyStatus();

       this.startAnimation();

       this.setCompletionStatus();
      },

      startAnimation: function() {
        // Track window scrolling
        $(window).scroll(function() {
            // The scroll top of the window
            var scrollTop = $(this).scrollTop();

            /* For each animation component check to see if it
               is in view and needs to be animated */
            $(".animation-component").each(function() {
                var elemHeight = $(this).height();
                var offSet = $(this).offset();
                /* If the scroll top is greater than the element offset
                   minus the element height divided by 2 */
                if (scrollTop >= (offSet.top - (elemHeight / 2))) {
                   var thisAnimation = $(this).find(".animation-wrap");
                   $(thisAnimation).addClass("slideDown");
                   $(thisAnimation).removeClass("hidden");
                }
            });
        });
      }

    });

    Adapt.register("animation", Animation);

	return Animation;

});
