/*
* adapt-barGraph
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {
    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var barGraph = ComponentView.extend({

		events: {

		},


        preRender: function() {

        },

        postRender: function() {

            this.setHeights();

            //console.log('ready status set');
            this.setReadyStatus();

            //console.log('complete status set');
            this.setCompletionStatus();
        },

        setHeights: function() {
            var graphHeight = $('.left-graph-container').outerHeight();
            $('.percentages').css('height', graphHeight + "px");

            var totalPercentHeight = 0;
            var numPercentPlaces = 6;
            $('.percentages> ul> li').each(function(index) {
                if (index < numPercentPlaces) {
                    totalPercentHeight += $(this).outerHeight();
                }
            });
            //console.log('totalPercentHeight ' + totalPercentHeight + ' graphHeight ' + graphHeight);
            var padding = (graphHeight - totalPercentHeight) / 10;
            //console.log('padding ' + padding);

            $('.percentages> ul> li').css('padding', padding + "px");

        }

    });

    Adapt.register("barGraph", barGraph);

	return barGraph;

});
