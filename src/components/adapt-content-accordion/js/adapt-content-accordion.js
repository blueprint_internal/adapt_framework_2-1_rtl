define(function(require) {
  var ComponentView = require('coreViews/componentView');
  var Adapt = require('coreJS/adapt');

  var Content_Accordion = ComponentView.extend({

    events: {
      'click .content-accordion-item-title' : 'toggleItem'
    },

    postRender: function() {
        this.setReadyStatus();
        var setCompletionOn = this.model.get('_setCompletionOn');
        if(setCompletionOn === 'inview') {
            var cssSelector = this.$('.component-widget').length > 0 ? '.component-widget'
                : (this.$('.component-instruction').length > 0 ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null));

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
        }
    },

    inview: function(event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            if (visiblePartY === 'top') {
                this._isVisibleTop = true;
            } else if (visiblePartY === 'bottom') {
                this._isVisibleBottom = true;
            } else {
                this._isVisibleTop = true;
                this._isVisibleBottom = true;
            }

            if (this._isVisibleTop && this._isVisibleBottom) {
                this.$(this.model.get('cssSelector')).off('inview');
                this.setCompletionStatus();
            }
        }
    },

    toggleItem: function (event) {
      event.preventDefault();
      this.$('.content-accordion-item-body').stop(true,true).slideUp(200);
      if (!$(event.currentTarget).hasClass('selected')) {
        this.$('.content-accordion-item-title').removeClass('selected');
        $(event.currentTarget).addClass('selected visited').siblings('.content-accordion-item-body').slideToggle(200);
        this.$('.content-accordion-item-title-icon').removeClass('icon-minus').addClass('icon-plus');
        $('.content-accordion-item-title-icon', event.currentTarget).removeClass('icon-plus').addClass('icon-minus');
        if ($(event.currentTarget).hasClass('content-accordion-item')) {
          this.setVisited($(event.currentTarget).index());
        } else {
          this.setVisited($(event.currentTarget).parent('.content-accordion-item').index());
        }
      } else {
        this.$('.content-accordion-item-title').removeClass('selected');
        $(event.currentTarget).removeClass('selected');
        $('.content-accordion-item-title-icon', event.currentTarget).removeClass('icon-minus').addClass('icon-plus');
      }
    },

    setVisited: function(index) {
      var item = this.model.get('_items')[index];
      item._isVisited = true;
      this.checkCompletionStatus();
    },

    getVisitedItems: function() {
      return _.filter(this.model.get('_items'), function(item) {
        return item._isVisited;
      });
    },

    checkCompletionStatus: function() {
      if (!this.model.get('_isComplete')) {
        if (this.getVisitedItems().length == this.model.get('_items').length) {
          this.setCompletionStatus();
        }
      }
    }

  });

  Adapt.register("content-accordion", Content_Accordion);

  return Content_Accordion;

});
