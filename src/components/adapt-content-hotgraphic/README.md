#adapt-content-hotgraphic

A custom hot graphic component that is based off the contrib-hotgraphic, but with some changes:

####Included "_setCompletionOn" attribute that accepts the values "" or "inview"; an empty value will default to normal behavior.

##Installation

Clone to local storage and delete the .git folder before use in projects.

###Usage

Once installed, the component can be used to display an image with clickable 'hot spot' elements. The location of these hot spots are defined within the content. When a hot spot is clicked, a pop-up window appears which displays an image and text.

Controls are provided to move between the next and previous hot spots via the pop-up window.

##Settings overview

For example an see example.json within this package.

Further settings for this component are:

####_component

This value must be: `content-hotgraphic`

####_classes

You can use this setting to add custom classes to your template and LESS file.

####_layout

This defines the position of the component in the block. Values can be `full`, `left` or `right`.

####mobileBody

This is optional body text that will be shown when viewed on mobile.

####_graphic

The main hot graphic image is defined within this element. This element should contain only one value for `src`, `alt` and `title`.

####src

Enter a path to the image. Paths should be relative to the src folder, e.g.

course/en/images/gqcq-1-large.gif

####alt

A value for alternative text can be entered here.

####title

Title text can be entered here for the image.

####_items

Multiple items can be entered. Each item represents one hot spot for this component and contains values for `title`, `body` and `_graphic`.

####title

This is the title text for a hot spot popup.

####body

This is the main text for a hot spot popup.

####_graphic

Each hotspot can contain an image. Details for the image are entered within this setting.

####src

Enter a path to the image. Paths should be relative to the src folder, e.g.

course/en/images/gqcq-1-large.gif

####alt

A value for alternative text can be entered here.

####title

This setting is for the title attribute on the image.

####strapline

Enter text for a strapline. This will be displayed when viewport size changes to the smallest range and is shown as a title above the image.

####_top

Each hot spot must contain `_top` and `_left` coordinates to position them on the hot graphic.

Enter the number of pixels this hot spot should be from the top border of the main graphic.

####_left

Enter the number of pixels this hot spot should be from the left border of the main graphic.
