
define(function(require) {
  var ComponentView = require('coreViews/componentView');
  var Adapt = require('coreJS/adapt');

  var ContentHotGraphic = ComponentView.extend({

    initialize: function() {
      this.listenTo(Adapt, 'remove', this.remove);
      this.listenTo(this.model, 'change:_isVisible', this.toggleVisibility);
      this.preRender();
      if (Adapt.device.screenSize=='large') {
        this.render();
      } else {
        this.reRender();
      }
    },

    events: function () {
      return {
        'click .content-hotgraphic-graphic-pin':'openHotGraphic',
        'click .content-hotgraphic-popup-done':'closeHotGraphic',
        'click .content-hotgraphic-popup-nav .back':'previousHotGraphic',
        'click .content-hotgraphic-popup-nav .next':'nextHotGraphic'
      }
    },

    preRender: function () {
      this.listenTo(Adapt, 'device:changed', this.reRender, this);

    },

    postRender: function() {
      this.$('.content-hotgraphic-widget').imageready(_.bind(function() {
        this.setReadyStatus();
        var setCompletionOn = this.model.get('_setCompletionOn');
        if(setCompletionOn === 'inview') {
          var cssSelector = this.$('.component-widget').length > 0 ? '.component-widget'
            : (this.$('.component-instruction').length > 0 ? '.component-instruction'
            : (this.$('.component-body').length > 0 ? '.component-body' : null));

            if (!cssSelector) {
              this.setCompletionStatus();
            } else {
              this.model.set('cssSelector', cssSelector);
              this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
        }
      }, this));
            this.setCompletionStatus();//instant completion marking
            
    },

    inview: function(event, visible, visiblePartX, visiblePartY) {
      if (visible) {
        if (visiblePartY === 'top') {
          this._isVisibleTop = true;
        } else if (visiblePartY === 'bottom') {
          this._isVisibleBottom = true;
        } else {
          this._isVisibleTop = true;
          this._isVisibleBottom = true;
        }
        if (this._isVisibleTop && this._isVisibleBottom) {
          this.$(this.model.get('cssSelector')).off('inview');
          this.setCompletionStatus();
        }
      }
    },

    reRender: function() {
      if (Adapt.device.screenSize != 'large') {
        this.replaceWithNarrative();
      }
    },

    replaceWithNarrative: function() {
      var Narrative = require('components/adapt-content-narrative/js/adapt-content-narrative');
      var model = this.prepareNarrativeModel();
      var newNarrative = new Narrative({model:model, $parent: this.options.$parent});
      newNarrative.reRender();
      newNarrative.setupNarrative();
      this.options.$parent.append(newNarrative.$el);
      Adapt.trigger('device:resize');
      this.remove();
    },

    prepareNarrativeModel: function() {
      var model = this.model;
      model.set('_component', 'content-narrative');
      model.set('_wasHotgraphic', true);
      model.set('originalBody', model.get('body'));
      if (model.get('mobileBody')) {
        model.set('body', model.get('mobileBody'));
      }
      return model;
    },

    applyNavigationClasses: function (index) {
      var $nav = this.$('.content-hotgraphic-popup-nav'),
          itemCount = this.$('.content-hotgraphic-item').length;

      $nav.removeClass('first').removeClass('last');
      if(index === 0) {
        this.$('.content-hotgraphic-popup-nav').addClass('first');
      } else if (index >= itemCount-1) {
        this.$('.content-hotgraphic-popup-nav').addClass('last');
      }
    },

    openHotGraphic: function (event) {
      event.preventDefault();
      var currentHotSpot = $(event.currentTarget).data('id');
      this.$('.content-hotgraphic-item').hide().removeClass('active');
      this.$('.'+currentHotSpot).show().addClass('active');
      var currentIndex = this.$('.content-hotgraphic-item.active').index();
      this.setVisited(currentIndex);
      this.$('.content-hotgraphic-popup-count .current').html(currentIndex+1);
      this.$('.content-hotgraphic-popup-count .total').html(this.$('.content-hotgraphic-item').length);
      this.$('.content-hotgraphic-popup').show();
      this.$('.content-hotgraphic-popup a.next').focus();
      this.applyNavigationClasses(currentIndex);
    },

    closeHotGraphic: function (event) {
      event.preventDefault();
      var currentIndex = this.$('.content-hotgraphic-item.active').index();
      this.$('.content-hotgraphic-popup').hide();
      this.$('.content-hotgraphic-item').eq(currentIndex).focus();
    },

    previousHotGraphic: function (event) {
      event.preventDefault();
      var currentIndex = this.$('.content-hotgraphic-item.active').index();
      if (currentIndex > 0) {
        this.$('.content-hotgraphic-item.active').hide().removeClass('active');
        this.$('.content-hotgraphic-item').eq(currentIndex-1).show().addClass('active');
        this.setVisited(currentIndex-1);
        this.$('.content-hotgraphic-popup-count .current').html(currentIndex);
      }
      this.applyNavigationClasses(currentIndex-1);
    },

    nextHotGraphic: function (event) {
      event.preventDefault();
      var currentIndex = this.$('.content-hotgraphic-item.active').index();
      if (currentIndex < (this.$('.content-hotgraphic-item').length-1)) {
        this.$('.content-hotgraphic-item.active').hide().removeClass('active');
        this.$('.content-hotgraphic-item').eq(currentIndex+1).show().addClass('active');
        this.setVisited(currentIndex+1);
        this.$('.content-hotgraphic-popup-count .current').html(currentIndex+2);
      }
      this.applyNavigationClasses(currentIndex+1);
    },

    setVisited: function(index) {
      var item = this.model.get('_items')[index];
      item._isVisited = true;
      this.$('.content-hotgraphic-graphic-pin').eq(index).addClass('visited');
      this.checkCompletionStatus();
    },

    getVisitedItems: function() {
      return _.filter(this.model.get('_items'), function(item) {
        return item._isVisited;
      });
    },

    checkCompletionStatus: function() {
      if (!this.model.get('_isComplete')) {
        if (this.getVisitedItems().length == this.model.get('_items').length) {
          this.setCompletionStatus();
        }
      }
    }

  });

  Adapt.register("content-hotgraphic", ContentHotGraphic);

  return ContentHotGraphic;

});
