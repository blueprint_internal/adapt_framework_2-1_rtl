#adapt-content-standard

This component strives to be the "go-to" component for simple text and graphic display within the Adapt Framework without being overtly complex or requiring chaining multiple components together.

The _graphic attribute of this component provides the needed requirements to display a normal image element, which is optimised for various devices. This component handles three different sized images which are cycled based upon your screen size giving each user the best experience possible.

*NOTE* This component borrows its graphic functionality from the contrib-graphic component.

##Installation

Clone the repo into your project:
        [project_name]\src\components

##Attributes

####_graphic

The image element found within normal HTML.

####isBeforeBody

Boolean value whether the image should be displayed before the body content; else defaults displaying after body and instruction content blocks.

####alt

The alt attribute of the image.

####title

The title attribute of the image.

####large

Large image path, can be relative or absolute.

####medium

Medium image path, can be relative or absolute.

####small

Small image path, can be relative or absolute.

##Example

See the example.json for structuring.

##Known Problems

None at this time.