/*
 * adapt-contrib-flipCard
 * License - https://github.com/CrediPointSolutions/adapt-contrib-flipCard/blob/master/LICENSE
 * Maintainers - Himanshu Rajotia <himanshu.rajotia@credipoint.com>, CrediPoint Solutions <git@credipoint.com>
 */
define(function(require) {
  var ComponentView = require("coreViews/componentView");
  var Adapt = require("coreJS/adapt");

  var FlipCard = ComponentView.extend({

    events: {
      "click .flipCard-widget .flipCard-item": "flipItem"
    },

    preRender: function() {
      this.listenTo(Adapt, "device:resize", this.reRender, this);
      this.listenTo(Adapt, "device:changed", this.reRender, this);
      this.resizeCardHeight();
    },

    // this is use to set ready status for current component on postRender.
    postRender: function() {
      ComponentView.prototype.postRender.apply(this);
      var self = this;
      this.$(".flipCard-item-frontImage:first").load(function() {
        self.reRender();
      });
      if (!Modernizr.csstransforms3d) {
        this.$(".flipCard-item-back").hide();
      }
      this.setReadyStatus();
      this.setCompletionStatus();//cheat and complete without any effort
    },

    // This function called on triggering of device resize and device change event of Adapt.
    reRender: function() {

    },

    resizeCardHeight: function() {
        var that = this;

        function resizeCard() {
            var thisBiggestHeight = 0;
            var absoluteBiggestHeight = 0;
            var thisHeight = 0;
            var lastHeight = 0;
            var rowHeight = [];

            // Get the height of the largest card and resize all cards to match height
            that.$(".flipCard-item").each(function(index) {
                var frontHeight1 = $(this).find('.flipCard-item-frontHeading').outerHeight(true);
                var frontHeight2 = $(this).find('.flipCard-item-frontOptions').outerHeight(true);
                var frontHeight3 = $(this).find('.flipCard-item-frontImage').outerHeight(true);
                var frontHeight4 = $(this).find('.flipCard-item-frontFooter').outerHeight(true);
                var totalFrontHeight = frontHeight1 + frontHeight2 + frontHeight3 + frontHeight4;

                var backHeight1 = $(this).find('.flipCard-item-backHeading').outerHeight(true);
                var backHeight2 = $(this).find('.flipCard-item-backOptions').outerHeight(true);
                var backHeight3 = $(this).find('.flipCard-item-backImage').outerHeight(true);
                var totalBackHeight = backHeight1 + backHeight2 + backHeight3;

                if (totalFrontHeight >= totalBackHeight) {
                  thisHeight = totalFrontHeight;
                } else {
                  thisHeight = totalBackHeight;
                }

                thisBiggestHeight = thisHeight;

                if (thisBiggestHeight > lastHeight) {
                  absoluteBiggestHeight = thisBiggestHeight;
                }

                if (index % 2 == 1 && index != 0) {
                    rowHeight.push(absoluteBiggestHeight);
                }

                lastHeight = thisBiggestHeight;

            });

            var indexItemPos = 0;
            $('.flipCard-item').each(function(index) {

                if (index % 2 == 0 && index != 0) {
                    indexItemPos++;
                }
                $(this).css('min-height', rowHeight[indexItemPos] + 'px');
            });
        }

        setTimeout(function() {
            resizeCard();
        },1000);

    },

    // Click or Touch event handler for flip card.
    flipItem: function(event) {
      event.preventDefault();
      var $selectedElement = $(event.target);
      var flipType = this.model.get("_flipType");
      if (flipType === "individualFlip") {
        this.performIndividualFlip($selectedElement);
      } else if (flipType === "singleFlip") {
        this.performSingleFlip($selectedElement);
      }
    },

    // This function will be responsible to perform Individual flip on flipCard
    // where all cards can flip and stay in the flipped state.
    performIndividualFlip: function($selectedElement) {
      var $flipCardItem;
      if ($selectedElement.hasClass("flipCard-item")) {
        $flipCardItem = $selectedElement;
      } else {
        $flipCardItem = $selectedElement.closest(".flipCard-item");
      }

      if (!Modernizr.csstransforms3d) {
        var $frontFlipCard = $flipCardItem.find(".flipCard-item-front");
        var $backFlipCard = $flipCardItem.find(".flipCard-item-back");
        var flipTime = this.model.get("_flipTime") | "fast";
        if ($frontFlipCard.is(":visible")) {
          $frontFlipCard.fadeOut(flipTime, function() {
            $backFlipCard.fadeIn(flipTime);
          });
        } else if ($backFlipCard.is(":visible")) {
          $backFlipCard.fadeOut(flipTime, function() {
            $frontFlipCard.fadeIn(flipTime);
          });
        }
      } else {
        $flipCardItem.toggleClass("flipCard-flip");
      }

      var flipCardElementIndex = this.$(".flipCard-item").index($flipCardItem);
      this.setVisited(flipCardElementIndex);
    },

    // This function will be responsible to perform Single flip on flipCard where
    // only one card can flip and stay in the flipped state.
    performSingleFlip: function($selectedElement) {
      var $flipCardItem;
      if ($selectedElement.hasClass("flipCard-item")) {
        $flipCardItem = $selectedElement;
      } else {
        $flipCardItem = $selectedElement.closest(".flipCard-item");
      }

      var flipCardContainer = $flipCardItem.closest(".flipCard-widget");
      if (!Modernizr.csstransforms3d) {
        var frontFlipCard = $flipCardItem.find(".flipCard-item-front");
        var backFlipCard = $flipCardItem.find(".flipCard-item-back");
        var flipTime = this.model.get("_flipTime") | "fast";

        if (backFlipCard.is(":visible")) {
          backFlipCard.fadeOut(flipTime, function() {
            frontFlipCard.fadeIn(flipTime);
          });
        } else {
          var visibleFlipCardBack = flipCardContainer.find(".flipCard-item-back:visible");
          if (visibleFlipCardBack.length > 0) {
            visibleFlipCardBack.fadeOut(flipTime, function() {
              flipCardContainer.find(".flipCard-item-front:hidden").fadeIn(flipTime);
            });
          }
          frontFlipCard.fadeOut(flipTime, function() {
            backFlipCard.fadeIn(flipTime);
          });
        }
      } else {
        if ($flipCardItem.hasClass("flipCard-flip")) {
          $flipCardItem.removeClass("flipCard-flip");
        } else {
          flipCardContainer.find(".flipCard-item").removeClass("flipCard-flip");
          $flipCardItem.addClass("flipCard-flip");
        }
      }

      var flipCardElementIndex = this.$(".flipCard-item").index($flipCardItem);
      this.setVisited(flipCardElementIndex);
    },

    // This function will set the visited status for particular flipCard item.
    setVisited: function(index) {
      var item = this.model.get("_items")[index];
      item._isVisited = true;
      //this.checkCompletionStatus();
    },

    // This function will be used to get visited states of all flipCard items.
    getVisitedItems: function() {
      return _.filter(this.model.get("_items"), function(item) {
        return item._isVisited;
      });
    },

    // This function will check or set the completion status of current component.
    checkCompletionStatus: function() {
      if (!this.model.get("_isComplete")) {
        //this.model.get("_items").length
        if (this.getVisitedItems().length === 1) {
          this.setCompletionStatus();
        }
      }
    }
  });

  Adapt.register("flipCard", FlipCard);

  return FlipCard;

});
