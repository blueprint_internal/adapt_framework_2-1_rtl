/*
* adapt-contrib-text
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley <darylhedley@hotmail.com>, Brian Quinn <brian@learningpool.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var mep = require('components/adapt-contrib-media/js/mediaelement-and-player');
    var FramedVideo = ComponentView.extend({
        preRender: function() {
           this.listenTo(Adapt, 'device:resize', this.onScreenSizeChanged);
            this.listenTo(Adapt, 'device:changed', this.onDeviceChanged); 
        },
        
        postRender: function() {
            this.setReadyStatus();
            
            //this.$('audio, video').mediaelementplayer('#vid');
            //this.$('.mejs-offscreen').hide();
            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction' 
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            
            var videoStyle = this.$(".framed-video-inner");
            if(this.model.get("_videoPosition") == "top")
            {
                videoStyle.css('z-index', '5');
            }
            videoStyle.css('padding-top', this.model.get("_videoPt").y);
            videoStyle.css('padding-left', this.model.get("_videoPt").x);

            if(this.model.get("_componentHeight")!=undefined)
            {
                this.$el.css('height',this.model.get("_componentHeight"));
            }
            if(this.model.get("_componentWidth")!=undefined)
            {
                this.$el.css('width',this.model.get("_componentWidth"));
            }

        },
        onDeviceChanged: function() {
           if(this.model.get("_componentWidth")!=undefined)
            {
                this.$el.css('width',this.model.get("_componentWidth"));
            }
        },
        onScreenSizeChanged: function() {
          
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            var vid = this.$("video");
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    
                    if(vid[0] != undefined)
                    {
                        vid[0].pause();
                        vid[0].currentTime = 0;
                        vid[0].play();
                        vid[0].controls = false;
                    }
                    var controls = this.model.get('_showControls');
                    var mute = this.model.get('_mute');
                    switch(controls)
                    {
                        case true:
                            vid[0].controls = true; 
                        break
                        case false:
                            vid[0].controls = false;
                        break
                    }
                    if(mute)
                    {
                       mute?vid[0].muted = true:vid[0].muted = false;
                    }
                    //vid[0].controls = true;
                    //vid[0].muted = false;
                    //this.$el.css({'video::-webkit-media-controls':'rotate(180deg)'});
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }               
            }
            else
            {
                /*
                if(vid[0] != undefined){
                    vid[0].pause();
                    vid[0].currentTime = 0;
                }
                */
            }
        }
        
    });
    
    Adapt.register("framed-video", FramedVideo);
    
});