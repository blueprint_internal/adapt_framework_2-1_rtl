#adapt-survey-checkbox

This component provides a list of selectable items where zero, one, or more items can be selected at any time.


##Installation

Clone the repo into your project:
        [project_name]\src\components

##Survey Components

Although these components look and function similar to existing components (textInput, mcq or gmcq) they inherit directly from the component class; this is different than standard question components which inherit from the question class and ties into the tutor system. Also unique to these survey components is the ability to detect content changes and store the new value automatically, and the ability to maintain state when navigating to different pages, articles, and components.

##Attributes

####userSelection (array of strings)

The user's selected values; this can be set beforehand.

####_items (array of objects)

Provides a listing of all available options.

####label (string)

A label to accompany the value; if provided, this will be displayed instead of the actual value.

####value (string)

REQUIRED for an item to appear as a selectable object; this is the actual value that will be stored.

####_graphic (object)

This object provides a graphic with the selection; uses a standard img element.

####alt (string)

An img element's alt attribute.

####title (string)

An img element's title attribute.

####src (string)

An img element's src attribute.

##Example

See the example.json for structuring.

##Known Problems

None at this time.

