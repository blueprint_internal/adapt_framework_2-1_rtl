/*
* adapt-survey-feedback
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var Component = ComponentView.extend({
		initialize: function() {
			this.listenTo(Adapt, 'survey:refresh', this.updateContent);
			this.listenTo(Adapt, 'remove', this.remove);
			this.listenTo(this.model, 'change:_isVisible', this.toggleVisibility);
			this.preRender();
			this.render();
		},

		showFeedback: function() {
			var feedback = this.model.get("feedback");
			var limitFeedback = this.model.get("limitFeedback");
			var responses = parseFeedbackConditions();
			this.model.set('responses', responses);

			function parseFeedbackConditions() {
				// Acceptable values: ["is", "is not", "contains"]
				var responses = [];
				for(var i = 0; i < feedback.length; i++) {
					var results = [];
					var resultsFor = feedback[i].resultsFor;
					var condition = feedback[i].condition.toLowerCase();
					var value = feedback[i].value;
					var caseSensitive = feedback[i].caseSensitive === true ? true : false;
					getSurveyResults_component(results, resultsFor);
					if(shouldSendResponse(results, condition, value, caseSensitive)) {
						addValueToArray(responses, feedback[i].response);
						if(limitFeedback === true) break;
					}
				}
				return responses;
			}

			function addValueToArray(array, value) {
				// if existing value, do not push to array
				for(var i = 0; i < array.length; i++) {
					if(array[i].value === value) {
						return;
					}
				}
				// if does not exist, push value to array
				array.push({"value":value});
				return;
			}

			function shouldSendResponse(results, condition, value, caseSensitive) {
				var bool = false;
				var resultValues;
				for(var i = 0; i < results.length; i++) {
					resultValues = results[i].value;
					loop(resultValues);
				}
				return bool;

				function loop(input){
					if( typeof input === "object" )
						for(var i = 0; i < input.length; i++){
							loop(input[i]);
						}
					else conditionals(input);
				}

				function conditionals(result){
					if( !caseSensitive ) {
						value = value.toLowerCase();
						result = result.toLowerCase();
					}
					if((condition === "is" && result == value)
					|| (condition === "is not" && result != value)
					|| (condition === "contains" && result.indexOf(value) != -1)
					) bool = true;
				}
			}

			function getSurveyResults_component(results, componentIds) {
				// results (array) byRef
				// componentIds (Adapt Attribute containing array)
				var models = Adapt.components.models;
				for(var i = 0; i < models.length; i++ ){
					for(var j = 0; j < componentIds.length; j++ ){
						if(models[i].get("_id") == componentIds[j]) {
							getSurveyResult(models[i], results);
						}
					}
				}
			}

			function getSurveyResult(model, results) {
				// model (Adapt Model)
				// results (array) byRef
				var attr = model.attributes;
				if(attr.userSelection) {
					results.push({"id":attr._id, "value":attr.userSelection});
				}
			}
		},

		showFeedbackBody: function() {
			var feedbackBody = this.model.get("feedbackBody");
			var responses = this.model.get("responses");
			var displayFeedbackBody = displayFeedbackBody(); // requires boolean value
			this.model.set('displayFeedbackBody', displayFeedbackBody);

			function displayFeedbackBody(){
				var bool = false;
				if( (typeof feedbackBody === "string" && feedbackBody != "")
					&& responses.length > 0
					) {
					bool = true
				}
				return bool;
			}
		},

		showFeedbackPostBody: function() {
			var feedbackPostBody = this.model.get("feedbackPostBody");
			var responses = this.model.get("responses");
			var displayFeedbackPostBody = displayFeedbackPostBody(); // requires boolean value
			this.model.set('displayFeedbackPostBody', displayFeedbackPostBody);

			function displayFeedbackPostBody(){
				var bool = false;
				if( (typeof feedbackPostBody === "string" && feedbackPostBody != "")
					&& responses.length > 0
					) {
					bool = true
				}
				return bool;
			}
		},

		updateContent: function(){
			this.preRender();
			this.render();
		},

		preRender: function() {
			this.showFeedback(); // Show/Update survey feedback
			this.showFeedbackBody(); // Show/Hide static text feedbackBody; must be called after showFeedback
			this.showFeedbackPostBody(); // Show/Hide static text feedbackPostBody; must be called after showFeedback
		},

		postRender: function() {
			this.setReadyStatus();

			// Check if instruction or body is set, otherwise force completion
			var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
				: (this.$('.component-body').length > 0 ? '.component-body' : null);

			if (!cssSelector) {
				this.setCompletionStatus();
			} else {
				this.model.set('cssSelector', cssSelector);
				this.$(cssSelector).on('inview', _.bind(this.inview, this));
			}
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
				}
			}
		}
	});
	Adapt.register("survey-feedback", Component);
});