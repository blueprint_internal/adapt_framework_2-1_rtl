/*
* adapt-survey-results
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var Component = ComponentView.extend({
		initialize: function() {
			this.listenTo(Adapt, 'survey:refresh', this.updateContent);
			this.listenTo(Adapt, 'remove', this.remove);
			this.listenTo(this.model, 'change:_isVisible', this.toggleVisibility);
			this.preRender();
			this.render();
		},

		showResults: function() {
			var resultsFor = this.model.get("resultsFor"); // String, allowable values are "course" (default), "page", or "component".
			var componentIds = this.model.get("componentIds"); // Array of comma separated component ids, applicable only with "component".
			var results = surveyResults(resultsFor, componentIds);
			this.model.set('results', results);

			function surveyResults(resultsFor, componentIds) {
				// resultsFor (Adapt Attribute containing string)
				// componentIds (Adapt Attribute containing array)
				var results = [];
				resultsFor = resultsFor.toLowerCase();
				if(resultsFor === "component") { getSurveyResults_component(results, componentIds); }
				else if(resultsFor === "page") { getSurveyResults_page(results); }
				else { getSurveyResults_course(results); }
				return results;
			}

			function getSurveyResults_course(results) {
				// results (array) byRef
				var children = [];
				var models = Adapt.contentObjects.models;
				for(var i = 0; i < models.length; i++ ){
					children = models[i].findDescendants('components');
					getSurveyResults(children, results);
				}
			}

			function getSurveyResults_page(results) {
				// results (array) byRef
				var model = this.model.getParent().getParent().getParent();
				var children = model.findDescendants('components');
				getSurveyResults(children, results);
			}


			function getSurveyResults_component(results, componentIds) {
				// results (array) byRef
				// componentIds (Adapt Attribute containing array)
				var models = Adapt.components.models;
				for(var i = 0; i < models.length; i++ ){
					for(var j = 0; j < componentIds.length; j++ ){
						if(models[i].get("_id") == componentIds[j]) {
							getSurveyResult(models[i], results);
						}
					}
				}
			}

			function getSurveyResults(components, results) {
				// components (Adapt Object)
				// results (array) byRef
				for(var i = 0; i < components.models.length; i++) {
					getSurveyResult(components.models[i], results);
				}
			}

			function getSurveyResult(model, results) {
				// model (Adapt Model)
				// results (array) byRef
				var attr = model.attributes;
				if(attr.userSelection) {
					if(typeof attr.userSelection === "object") {
						results.push({"id":attr._id, "values":attr.userSelection, "question":attr.body});
					}
					else results.push({"id":attr._id, "value":attr.userSelection, "question":attr.body});
				}
			}
		},

		showResultsBody: function() {
			var resultsBody = this.model.get("resultsBody");
			var results = this.model.get("results");
			var displayResultsBody = displayResultsBody(); // requires boolean value
			this.model.set('displayResultsBody', displayResultsBody);

			function displayResultsBody(){
				var bool = false;
				if( (typeof resultsBody === "string" && resultsBody != "")
					&& results.length > 0
					) {
					bool = true
				}
				return bool;
			}
		},

		showResultsPostBody: function() {
			var resultsPostBody = this.model.get("resultsPostBody");
			var results = this.model.get("results");
			var displayResultsPostBody = displayResultsPostBody(); // requires boolean value
			this.model.set('displayResultsPostBody', displayResultsPostBody);

			function displayResultsPostBody(){
				var bool = false;
				if( (typeof resultsPostBody === "string" && resultsPostBody != "")
					&& results.length > 0
					) {
					bool = true
				}
				return bool;
			}
		},

		updateContent: function(){
			this.preRender();
			this.render();
		},

		preRender: function() {
			this.showResults(); // Show/Update survey results
			this.showResultsBody(); // Show/Hide static text resultsBody; must be called after showResults
			this.showResultsPostBody(); // Show/Hide static text resultsPostBody; must be called after showResults
		},

		postRender: function() {
			this.setReadyStatus();

			// Check if instruction or body is set, otherwise force completion
			var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
				: (this.$('.component-body').length > 0 ? '.component-body' : null);

			if (!cssSelector) {
				this.setCompletionStatus();
			} else {
				this.model.set('cssSelector', cssSelector);
				this.$(cssSelector).on('inview', _.bind(this.inview, this));
			}
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
				}
			}
		}
	});
	Adapt.register("survey-results", Component);
});