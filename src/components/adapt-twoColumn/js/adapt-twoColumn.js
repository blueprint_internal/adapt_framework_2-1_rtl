/*
* adapt-textOverlay
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var twoColumn = ComponentView.extend({

		events: {

		},


        preRender: function() {

        },

        postRender: function() {
            this.setBorder();
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        },

        setBorder: function() {
            var border = "1px solid " + this.model.get('borderColor');

            function getGreaterHeight() {
                var leftColHeight = this.$('.twoCol-leftCol').outerHeight(true);
                var rightColHeight = this.$('.twoCol-rightCol').outerHeight(true);

                if (border) {
                    if (leftColHeight >= rightColHeight) {
                        this.$('.twoCol-leftCol').css('border-right', border);
                    } else {
                        this.$('.twoCol-rightCol').css('border-left', border);
                    }
                }
            }

            setTimeout(function() {
                getGreaterHeight();
            },500);
        }

    });

    Adapt.register("twoColumn", twoColumn);
	return twoColumn;

});
