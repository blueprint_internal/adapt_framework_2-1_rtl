/*
* adapt-textOverlay
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var video = ComponentView.extend({

		events: {
            "hover .3colVid": "showVideoCtl",
            "click .mute-video": "muteVideo",
            "click video": "syncMuteBtns"
		},


        preRender: function() {
            this.$el.addClass("no-state");
            // Checks to see if the blank should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        postRender: function() {
            this.trigerVideoClick();
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        },

        showVideoCtl: function() {
            this.$('.3colVid').hover(function toggleControls() {
                if (this.hasAttribute("controls")) {
                    this.removeAttribute("controls")
                } else {
                    this.setAttribute("controls", "controls")
                }
            })
        },

        muteVideo: function(e) {
            var muteBtnID = '#' + $(e.currentTarget).attr('id');
            var videoID = '#' + $(muteBtnID).parent().prev().attr('id');
            var muteBtnImg = '#' + $(muteBtnID).children().attr('id');
            if ($(videoID).prop('muted')) {
                $(videoID).prop('muted', false);
                $(muteBtnImg).attr('src', 'course/en/images/unmute.png');
            } else {
                $(videoID).prop('muted', true);
                $(muteBtnImg).attr('src', 'course/en/images/mute.png');
            }

        },

        syncMuteBtns: function(e) {
            var thisVid = "#" + $(e.currentTarget).attr('id');
            var muteBtnImgSync = '#' + $(thisVid).next().children().children().attr('id');
            var muted = $(thisVid).prop('muted');

            $(thisVid).bind('volumechange',function() {
                if ($(thisVid).prop('muted')) {
                    $(muteBtnImgSync).attr('src', 'course/en/images/mute.png');
                } else {
                    $(muteBtnImgSync).attr('src', 'course/en/images/unmute.png');
                }
            });
        },

        trigerVideoClick: function() {
            $('video').each(function() {
                $(this).click();
            });
        }

    });



    Adapt.register("video", video);
	return video;

});
